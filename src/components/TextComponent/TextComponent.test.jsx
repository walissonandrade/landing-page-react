import { screen } from '@testing-library/react';
import { renderTheme } from '../../styles/renderTheme';
import { TextComponent } from '.';
import { theme } from '../../styles/theme';

describe('TextComponent', () => {
  it('should render a text', () => {
    renderTheme(<TextComponent>Texto</TextComponent>);
    const textComponent = screen.getByText('Texto');

    expect(textComponent).toHaveStyle({
      'font-size': theme.font.sizes.medium,
    });
  });
});
