import { screen } from '@testing-library/react';
import { renderTheme } from '../../styles/renderTheme';
import { NavLinks } from '.';
import mock from './mock';
import { theme } from '../../styles/theme';

describe('NavLinks', () => {
  it('should render links', () => {
    renderTheme(<NavLinks links={mock} />);
    const navLinks = screen.getAllByRole('link');

    expect(navLinks).toHaveLength(mock.length);
  });

  it('should not render links', () => {
    renderTheme(<NavLinks />);
    const navLinks = screen.queryAllByText(/link/i);

    expect(navLinks).toHaveLength(0);
  });

  it('should render links', () => {
    renderTheme(<NavLinks links={mock} />);
    const navLinks = screen.getByText(/link 4/i).parentElement;

    expect(navLinks).toHaveStyleRule('flex-flow', 'column wrap', {
      media: theme.media.medium,
    });
  });

  it('should match snapshot', () => {
    const { container } = renderTheme(<NavLinks links={mock} />);
    expect(container).toMatchSnapshot();
  });
});
