import { screen } from '@testing-library/react';
import { GridTwoColumn } from '.';
import { renderTheme } from '../../styles/renderTheme';

import mock from './mock';

describe('GridTwoColumn', () => {
  it('should render component', () => {
    const { container } = renderTheme(<GridTwoColumn {...mock} />);
    const gridTwoColumn = screen.getByRole('heading');
    expect(gridTwoColumn).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
