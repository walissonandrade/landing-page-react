import P from 'prop-types';
import { Container, TextContainer, ImageContainer, Image } from './styled';
import { SectionBackground } from '../SectionBackground';
import { Heading } from '../Heading';
import { TextComponent } from '../TextComponent';

export const GridTwoColumn = ({
  title,
  text,
  srcImg,
  isBackgroundSection = false,
  sectionId = '',
}) => {
  return (
    <SectionBackground
      isBackgroundSection={isBackgroundSection}
      sectionId={sectionId}
    >
      <Container>
        <TextContainer>
          <Heading as="h2" uppercase colorDark={!isBackgroundSection}>
            {title}
          </Heading>
          <TextComponent>{text}</TextComponent>
        </TextContainer>
        <ImageContainer>
          <Image src={srcImg} alt={title} />
        </ImageContainer>
      </Container>
    </SectionBackground>
  );
};

GridTwoColumn.propTypes = {
  title: P.string.isRequired,
  text: P.string.isRequired,
  srcImg: P.string.isRequired,
  isBackgroundSection: P.bool,
  sectionId: P.string,
};
