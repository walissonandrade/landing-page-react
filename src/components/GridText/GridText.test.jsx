import { screen } from '@testing-library/react';
import { GridText } from '.';
import { renderTheme } from '../../styles/renderTheme';

import mock from './mock';

describe('GridText', () => {
  it('should render', () => {
    const { container } = renderTheme(<GridText {...mock} />);

    expect(container).toMatchSnapshot();
  });

  it('should render no background', () => {
    const { container } = renderTheme(
      <GridText {...mock} isBackgroundSection={undefined} />,
    );

    expect(container).toMatchSnapshot();
  });
});
