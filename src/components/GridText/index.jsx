import P from 'prop-types';
import { Container, Grid, GridElement } from './styled';
import { SectionBackground } from '../SectionBackground';
import { Heading } from '../Heading';
import { TextComponent } from '../TextComponent';

export const GridText = ({
  title,
  description,
  grid,
  isBackgroundSection = false,
  sectionId = '',
}) => {
  return (
    <SectionBackground
      isBackgroundSection={isBackgroundSection}
      sectionId={sectionId}
    >
      <Container>
        <Heading as="h2" size="huge" uppercase colorDark={!isBackgroundSection}>
          {title}
        </Heading>
        <TextComponent>{description}</TextComponent>
        <Grid>
          {grid.map((el) => (
            <GridElement key={el.title}>
              <Heading as="h3" size="medium" colorDark={!isBackgroundSection}>
                {el.title}
              </Heading>
              <TextComponent>{el.description}</TextComponent>
            </GridElement>
          ))}
        </Grid>
      </Container>
    </SectionBackground>
  );
};

GridText.propTypes = {
  title: P.string.isRequired,
  isBackgroundSection: P.bool,
  description: P.string.isRequired,
  grid: P.arrayOf(
    P.shape({
      title: P.string.isRequired,
      description: P.string.isRequired,
    }),
  ).isRequired,
  sectionId: P.string,
};
