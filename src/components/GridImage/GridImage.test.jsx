import { GridImage } from '.';
import { renderTheme } from '../../styles/renderTheme';

import mock from './mock';

describe('GridImage', () => {
  it('should render', () => {
    const { container } = renderTheme(<GridImage {...mock} />);

    expect(container).toMatchSnapshot();
  });

  it('should render no background', () => {
    const { container } = renderTheme(
      <GridImage {...mock} isBackgroundSection={undefined} />,
    );

    expect(container).toMatchSnapshot();
  });
});
