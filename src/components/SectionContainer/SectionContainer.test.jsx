import { screen } from '@testing-library/react';
import { renderTheme } from '../../styles/renderTheme';
import { SectionContainer } from '.';

describe('SectionContainer', () => {
  it('should render', () => {
    const { container } = renderTheme(
      <SectionContainer>
        <h1>Texto</h1>
      </SectionContainer>,
    );
    const sectionContainer = screen.getByRole('heading');

    expect(sectionContainer).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
