import { screen } from '@testing-library/react';
import { renderTheme } from '../../styles/renderTheme';
import { LogoLink } from '.';

describe('LogoLink', () => {
  it('should render text logo', () => {
    renderTheme(<LogoLink link="#target" text="texto" />);
    const logoLink = screen.getByRole('link', { name: 'texto' });

    expect(logoLink).toHaveAttribute('href', '#target');
  });

  it('should render image logo', () => {
    renderTheme(<LogoLink link="#target" text="texto" srcImage="image.jpg" />);
    const logoLink = screen.getByRole('link', { name: 'texto' });

    expect(logoLink.firstChild).toHaveAttribute('src', 'image.jpg');
  });
});
