import P from 'prop-types';
import { Container, Html } from './styled';
import { SectionBackground } from '../SectionBackground';
import { Heading } from '../Heading';
import { TextComponent } from '../TextComponent';

export const GridContent = ({
  title,
  html,
  isBackgroundSection = false,
  sectionId = '',
}) => {
  return (
    <SectionBackground
      isBackgroundSection={isBackgroundSection}
      sectionId={sectionId}
    >
      <Container>
        <Heading as="h2" uppercase colorDark={!isBackgroundSection}>
          {title}
        </Heading>
        <Html>
          <TextComponent>{html}</TextComponent>
        </Html>
      </Container>
    </SectionBackground>
  );
};

GridContent.propTypes = {
  title: P.string.isRequired,
  html: P.string.isRequired,
  isBackgroundSection: P.bool,
  sectionId: P.string,
};
