import { screen } from '@testing-library/react';
import { GridContent } from '.';
import { renderTheme } from '../../styles/renderTheme';

import mock from './mock';

describe('GridContent', () => {
  it('should render Component GridContent', () => {
    const { container } = renderTheme(<GridContent {...mock} />);

    expect(container).toMatchSnapshot();
  });

  it('should render Component GridContent', () => {
    const { container } = renderTheme(
      <GridContent {...mock} isBackgroundSection={undefined} />,
    );

    expect(container).toMatchSnapshot();
  });
});
