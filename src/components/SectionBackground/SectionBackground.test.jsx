import { screen } from '@testing-library/react';
import { renderTheme } from '../../styles/renderTheme';
import { SectionBackground } from '.';

describe('SectionBackground', () => {
  it('should render with background dark', () => {
    const { container } = renderTheme(
      <SectionBackground isBackgroundSection>
        <h1>Texto</h1>
      </SectionBackground>,
    );
    const sectionBackground = screen.getByRole('heading');

    expect(sectionBackground).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('should render with background ligth', () => {
    const { container } = renderTheme(
      <SectionBackground>
        <h1>Texto</h1>
      </SectionBackground>,
    );
    const sectionBackground = screen.getByRole('heading');

    expect(sectionBackground).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
