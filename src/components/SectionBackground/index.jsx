import P from 'prop-types';
import { Container } from './styled';
import { SectionContainer } from '../SectionContainer';

export const SectionBackground = ({
  children,
  isBackgroundSection = false,
  sectionId = '',
}) => {
  return (
    <Container isBackgroundSection={isBackgroundSection} id={sectionId}>
      <SectionContainer>{children}</SectionContainer>
    </Container>
  );
};

SectionBackground.propTypes = {
  children: P.node.isRequired,
  isBackgroundSection: P.bool,
  sectionId: P.string,
};
