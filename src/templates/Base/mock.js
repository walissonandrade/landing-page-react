import linksMock from '../../components/NavLinks/mock';
import { GridText } from '../../components/GridText';
import gridMock from '../../components/GridText/mock';

export default {
  links: linksMock,
  logoData: {
    text: 'Logo',
    link: '#',
  },
  footerHtml: '<p>teste de footer</p>',
  children: (
    <>
      <GridText {...gridMock} />
      <GridText {...gridMock} isBackgroundSection />
      <GridText {...gridMock} />
      <GridText {...gridMock} isBackgroundSection />
      <GridText {...gridMock} />
      <GridText {...gridMock} isBackgroundSection />
    </>
  ),
};
