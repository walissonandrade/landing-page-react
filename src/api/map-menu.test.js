import { mapMenu, mapMenuLinks } from './map-menu';

describe('map-menu', () => {
  it('should return a predefined object if no data', () => {
    const menu = mapMenu();
    expect(menu.newTab).toBe(false);
    expect(menu.text).toBe('');
    expect(menu.srcImg).toBe('');
    expect(menu.link).toBe('');
  });

  it('should map menu data', () => {
    const menu = mapMenu({
      open_in_new_tab: false,
      _id: '602fdf30540c00269e0561ae',
      logo_text: 'Landing Page',
      logo_link: '#home',
      menu: [
        {
          open_in_new_tab: false,
          _id: '602fdf30540c00269e0561b3',
          link_text: 'pricing',
          url: '#pricing',
          __v: 0,
          id: '602fdf30540c00269e0561b3',
        },
        {
          open_in_new_tab: false,
          _id: '602fdf30540c00269e0561b4',
          link_text: 'contact',
          url: '#contact',
          __v: 0,
          id: '602fdf30540c00269e0561b4',
        },
      ],
      __v: 1,
      logo: {
        url: 'a.svg',
      },
    });

    expect(menu.newTab).toBe(false);
    expect(menu.text).toBe('Landing Page');
    expect(menu.srcImg).toBe('a.svg');
    expect(menu.link).toBe('#home');
    expect(menu.links[0].newTab).toBe(false);
    expect(menu.links[0].children).toBe('pricing');
    expect(menu.links[0].link).toBe('#pricing');
  });

  it('should return an empty array if no links', () => {
    const links = mapMenuLinks();
    expect(links).toEqual([]);
  });

  it('should return links if links passed', () => {
    const links = mapMenuLinks([
      {
        open_in_new_tab: false,
        link_text: 'pricing',
        url: '#pricing',
      },
      {},
    ]);

    expect(links[0].newTab).toBe(false);
    expect(links[0].children).toBe('pricing');
    expect(links[0].link).toBe('#pricing');
  });
});
