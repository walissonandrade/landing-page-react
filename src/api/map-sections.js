export const mapSections = (sections = []) => {
  return sections.map((section) => {
    if (section._component === 'section.section-two-columns') {
      return mapSectionTwoColumns(section);
    } else if (section._component === 'section.section-content') {
      return mapSectionContent(section);
    } else if (section._component === 'section.section-grid') {
      const { text_grid = [], image_grid = [] } = section;
      if (text_grid.length > 0) {
        return mapTextGrid(section);
      } else if (image_grid.length > 0) {
        return mapImageGrid(section);
      }
    }
  });
};

export const mapSectionTwoColumns = (section = {}) => {
  const {
    __component: component = '',
    title = '',
    description: text = '',
    image: { url: srcImg = '' } = '',
    metadata: {
      background: isBackgroundSection = false,
      section_id: sectionId = '',
    } = false,
  } = section;

  return {
    component,
    title,
    text,
    srcImg,
    isBackgroundSection,
    sectionId,
  };
};

export const mapSectionContent = (section = {}) => {
  const {
    __component: component = '',
    title = '',
    content: html = '',
    metadata: {
      background: isBackgroundSection = false,
      section_id: sectionId = '',
    } = false,
  } = section;

  return {
    component,
    title,
    html,
    isBackgroundSection,
    sectionId,
  };
};

export const mapTextGrid = (section = {}) => {
  const {
    __component: component = '',
    title = '',
    description = '',
    metadata: {
      background: isBackgroundSection = false,
      section_id: sectionId = '',
    } = false,
    text_grid: grid = [],
  } = section;

  return {
    component: 'section.section-grid-text',
    title,
    description,
    isBackgroundSection,
    sectionId,
    grid: grid.map((text) => {
      const { title = '', description = '' } = text;
      return {
        title,
        description,
      };
    }),
  };
};

export const mapImageGrid = (section = {}) => {
  const {
    __component: component = '',
    title = '',
    description = '',
    metadata: {
      background: isBackgroundSection = false,
      section_id: sectionId = '',
    } = false,
    image_grid: grid = [],
  } = section;

  return {
    component: 'section.section-grid-image',
    title,
    description,
    isBackgroundSection,
    sectionId,
    grid: grid.map((img) => {
      const {
        image: { url: srcImg = '', alternativeText: altText = '' } = '',
      } = img;
      return {
        srcImg,
        altText,
      };
    }),
  };
};
