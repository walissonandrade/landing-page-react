import {
  mapImageGrid,
  mapSectionContent,
  mapSections,
  mapSectionTwoColumns,
  mapTextGrid,
} from './map-sections';

describe('map-sections', () => {
  it('should render predefined section if no data', () => {
    const data = mapSections();
    expect(data).toEqual([]);
  });

  it('should map section two columns', () => {
    const data = mapSectionTwoColumns();

    expect(data.isBackgroundSection).toBe(false);
    expect(data.component).toBe('');
    expect(data.sectionId).toBe('');
    expect(data.srcImg).toBe('');
    expect(data.text).toBe('');
    expect(data.title).toBe('');

    const data2 = mapSectionTwoColumns({
      __component: 'section.section-two-columns',
      title: 'January brings us Firefox 85',
      description: 'To wrap',
      metadata: {
        background: true,
        section_id: 'home',
      },
      __v: 1,
      image: {
        url: 'a.svg',
      },
    });

    expect(data2.isBackgroundSection).toBe(true);
    expect(data2.component).toBe('section.section-two-columns');
    expect(data2.sectionId).toBe('home');
    expect(data2.srcImg).toBe('a.svg');
    expect(data2.text).toBe('To wrap');
    expect(data2.title).toBe('January brings us Firefox 85');
  });

  it('should map section content', () => {
    const data = mapSectionContent();
    expect(data.isBackgroundSection).toBe(false);
    expect(data.component).toBe('');
    expect(data.sectionId).toBe('');
    expect(data.html).toBe('');
    expect(data.title).toBe('');

    const data2 = mapSectionContent({
      __component: 'section.section-content',
      title: 'news coverage and some surprises',
      content: 'Apple Silico',
      metadata: {
        background: false,
        name: 'intro',
        section_id: 'intro',
      },
    });

    expect(data2.isBackgroundSection).toBe(false);
    expect(data2.component).toBe('section.section-content');
    expect(data2.sectionId).toBe('intro');
    expect(data2.html).toBe('Apple Silico');
    expect(data2.title).toBe('news coverage and some surprises');
  });

  it('should map grid text', () => {
    const data = mapTextGrid();
    expect(data.isBackgroundSection).toBe(false);
    expect(data.component).toBe('section.section-grid-text');
    expect(data.sectionId).toBe('');
    expect(data.description).toBe('');
    expect(data.title).toBe('');
    expect(data.grid).toEqual([]);

    const data2 = mapTextGrid({
      __component: 'section.section-grid',
      description: 'Debitis cum',
      title: 'My Grid',
      text_grid: [
        {
          title: 'Teste 1',
          description: 'Lorem',
        },
        {
          title: 'Teste 2',
          description: 'Lorem',
        },
        {
          title: 'Teste 3',
          description: 'Lorem',
        },
      ],
      image_grid: [],
      metadata: {
        background: true,
        name: 'grid-one',
        section_id: 'grid-one',
      },
    });

    expect(data2.isBackgroundSection).toBe(true);
    expect(data2.component).toBe('section.section-grid-text');
    expect(data2.sectionId).toBe('grid-one');
    expect(data2.description).toBe('Debitis cum');
    expect(data2.title).toBe('My Grid');
    expect(data2.grid[0].title).toBe('Teste 1');
    expect(data2.grid[0].description).toBe('Lorem');
  });

  it('should map grid image', () => {
    const data = mapImageGrid();
    expect(data.isBackgroundSection).toBe(false);
    expect(data.component).toBe('section.section-grid-image');
    expect(data.sectionId).toBe('');
    expect(data.description).toBe('');
    expect(data.title).toBe('');
    expect(data.grid).toEqual([]);

    const data2 = mapImageGrid({
      __component: 'section.section-grid',
      description: 'Distinctio',
      title: 'Gallery',
      text_grid: [],
      image_grid: [
        {
          image: {
            alternativeText: 'Uma paisagem com céu claro e montanhas bonitas',
            url: 'a.jpg',
            provider_metadata: {
              public_id: '360x360_r_1_6a2049d13a',
              resource_type: 'image',
            },
          },
        },
        {
          image: {
            alternativeText: 'Um livro grande aberto',
            url: 'dsfsdf.jpg',
          },
        },
      ],
      metadata: {
        background: false,
        name: 'gallery',
        section_id: 'gallery',
      },
    });
    expect(data2.isBackgroundSection).toBe(false);
    expect(data2.component).toBe('section.section-grid-image');
    expect(data2.sectionId).toBe('gallery');
    expect(data2.description).toBe('Distinctio');
    expect(data2.title).toBe('Gallery');
    expect(data2.grid[0].srcImg).toBe('a.jpg');
    expect(data2.grid[0].altText).toBe(
      'Uma paisagem com céu claro e montanhas bonitas',
    );
  });
});
